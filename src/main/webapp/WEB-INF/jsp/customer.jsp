<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
</head>
<body>
	<center>
		<c:if test="${message != null}">
			<h4>
				<div style="color: green;">${message}</div>
			</h4>
		</c:if>

		<c:if test="${customers != null}">
			<br />
			<br />

			<h2>Search Customer</h2>
			<form id="searchCustomer" name='search' action="search" method='POST'
				modelAttribute="customer">
				<table>
					<tr>
						<td>Customer Name:</td>
						<td><input id="searchCustomerName" type='text'
							name='customerName'></td>
						<td>Customer City:</td>
						<td><input type='text' name='CustomerCity'
							id="searchCustomerCity" /></td>
						<td><input name="submit" type="submit" value="Search"
							style="color: DodgerBlue;" /></td>
					</tr>
				</table>
			</form>
			<br />
			<br />
			<table style="width: 80%" border="1">
				<tr>
					<td>Customer Id</td>
					<td>Customer Name</td>
					<td>Customer City</td>
					<td>Customer Balance</td>
					<td>Status</td>
					<td>Action</td>
				</tr>

				<c:forEach items="${customers}" var="customer">
					<tr>
						<td><c:out value="${customer.customerId}" /></td>
						<td><c:out value="${customer.customerName}" /></td>
						<td><c:out value="${customer.customerCity}" /></td>
						<td><c:out value="${customer.customerBalance}" /></td>
						<td><c:choose>
								<c:when test="${customer.status}">Active</c:when>
								<c:otherwise> In-Active</c:otherwise>
							</c:choose></td>
						<td><a href="#"
							onclick="update('${customer.customerId}','${customer.customerName}','${customer.customerCity}','${customer.customerBalance}','${customer.status}')">edit</a>/<c:choose>
								<c:when test="${customer.status}">
									<a href="deActivate/${customer.customerId}">In-Activate</a>
								</c:when>
								<c:otherwise>
									<a href="activate/${customer.customerId}">Activate</a>
								</c:otherwise>
							</c:choose></td>
					</tr>
				</c:forEach>

			</table>
			<br />
			<br />
		</c:if>
		<form id="addCustomer" name='add' action="add" method='POST'
			modelAttribute="customer">
			<h2>Add Customer</h2>
			<table>
				<tr>
					<td>Customer Name:</td>
					<td><input id="customerName" type='text' name='customerName'>
					</td>

					<td>Customer City:</td>
					<td><input type='text' name='CustomerCity' id="CustomerCity" /></td>
				</tr>

				<tr>
					<td>Customer Balance:</td>
					<td><input type='number' name='CustomerBalance'
						id="CustomerBalance" /></td>
				</tr>
				<tr>
					<td><input name="submit" type="submit" value="Add Customer"
						style="color: DodgerBlue;" /></td>
				</tr>
			</table>
		</form>
		<form id="updateCustomer" name='add' action="update" method='POST'
			modelAttribute="customer" style="display: none">
			<h2>Update Customer</h2>
			<table>
				<tr>
					<td>Customer Name:</td>
					<td><input id="customerNameUpdate" type='text'
						name='customerName'> <input type="hidden"
						name="customerId" id="customerIdUpdate" value=0> <input
						type="hidden" name="status" id="statusUpdate"></td>

					<td>Customer City:</td>
					<td><input type='text' name='CustomerCity'
						id="CustomerCityUpdate" /></td>
				</tr>

				<tr>
					<td>Customer Balance:</td>
					<td><input type='number' name='CustomerBalance'
						id="CustomerBalanceUpdate" /></td>
				</tr>
				<tr>
					<td><input name="submit" type="submit" value="Update Customer"
						style="color: DodgerBlue;" /></td>
				</tr>
			</table>
		</form>
</body>
<script type="text/javascript">
	function update(customerId, customerName, CustomerCity, CustomerBalance,
			status) {
		$('#customerIdUpdate').val(customerId)
		$('#customerNameUpdate').val(customerName)
		$('#CustomerCityUpdate').val(CustomerCity)
		$('#CustomerBalanceUpdate').val(CustomerBalance)
		$('#statusUpdate').val(status)
		$("#addCustomer").css("display", "none");
		$("#updateCustomer").css("display", "block");

	}
</script>
</html>