package com.luxshsolution.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.luxshsolution.demo.entity.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
	
	@Query("SELECT c FROM Customer c") //WHERE STATUS = 1
	List<Customer> getAll();
	
	@Query("SELECT c FROM Customer c WHERE customerName = ?1 AND STATUS = 1")
	List<Customer> findByCustomerName(String customerName);
	
	@Query("SELECT c FROM Customer c WHERE customerCity = ?1 AND STATUS = 1")
	List<Customer> findByCustomerCity(String customerCity);
	
	@Query("SELECT c FROM Customer c WHERE customerName = ?1 AND customerCity = ?2 AND STATUS = 1")
	List<Customer> findByCustomerNameAndCustomerCity(String customerName, String customerCity);

}
