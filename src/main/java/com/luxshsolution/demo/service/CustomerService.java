package com.luxshsolution.demo.service;

import java.util.List;

import com.luxshsolution.demo.entity.Customer;

public interface CustomerService {

	List<Customer> getAll() throws Exception;

	List<Customer> search(Customer customer) throws Exception;

	void add(Customer customer) throws Exception;

	void update(Customer customer) throws Exception;

	void activate(int customerId) throws Exception;

	void deActivate(int customerId) throws Exception;

}
