package com.luxshsolution.demo.service.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luxshsolution.demo.entity.Customer;
import com.luxshsolution.demo.repository.CustomerRepository;
import com.luxshsolution.demo.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public List<Customer> getAll() throws Exception {
		List<Customer> customers = null;
		try {
			customers = customerRepository.getAll();
		} catch (Exception e) {
			throw e;
		}
		return customers;
	}

	@Override
	public List<Customer> search(Customer customer) throws Exception {
		List<Customer> customers = null;
		try {
				
			if(customer.getCustomerName() != null && !customer.getCustomerName().isBlank() && customer.getCustomerCity() != null && !customer.getCustomerCity().isBlank()) {
				customers = customerRepository.findByCustomerNameAndCustomerCity(customer.getCustomerName(), customer.getCustomerCity());
			} else if(customer.getCustomerName() != null && !customer.getCustomerName().isBlank()) {
				customers = customerRepository.findByCustomerName(customer.getCustomerName());
			} else if(customer.getCustomerCity() != null && !customer.getCustomerCity().isBlank()) {
				customers = customerRepository.findByCustomerCity(customer.getCustomerCity());
			}

		} catch (Exception e) {
			throw e;
		}
		return customers;
	}

	@Override
	public void add(Customer customer) throws Exception {
		try {
			customer.setStatus(true);
			customerRepository.save(customer);

		} catch (Exception e) {
			throw e;
		}

	}

	@Override
	public void update(Customer customer) throws Exception {
		try {
			customerRepository.save(customer);

		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void activate(int customerId) throws Exception {
		try {
			Customer customer = customerRepository.findById(customerId).orElseThrow();
			customer.setStatus(true);
			update(customer);

		} catch (Exception e) {
			throw e;
		}
		
	}

	@Override
	public void deActivate(int customerId) throws Exception {
		try {
			Customer customer = customerRepository.findById(customerId).orElseThrow();
			customer.setStatus(false);
			update(customer);

		} catch (Exception e) {
			throw e;
		}
	}
}
