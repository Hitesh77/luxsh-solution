package com.luxshsolution.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.luxshsolution.demo.entity.Customer;
import com.luxshsolution.demo.service.CustomerService;

@Controller
@RequestMapping(value = "customers")
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView get(@RequestParam(name = "request", required = false) String queryParam) throws Exception {
		ModelAndView view = new ModelAndView("customer");
		List<Customer> customers = customerService.getAll();
		String message = null;
		if(queryParam != null && !queryParam.isBlank()) {
			if(queryParam.equals("add")) {
				message = "Customer Added successfully.";
			} else if(queryParam.equals("update")) {
				message = "Customer details updated successfully.";
			} else if(queryParam.equals("activate")) {
				message = "Customer Activate successfully.";
			} else if(queryParam.equals("deactivate")) {
				message = "Customer Deactivate successfully.";
			}
		}
		view.addObject("customers", customers);
		view.addObject("message", message);
		return view;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ModelAndView add(@ModelAttribute("customer") Customer customer) throws Exception {
		ModelAndView view = new ModelAndView("redirect:/customers/?request=add");
		customerService.add(customer);
		return view;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView update(@ModelAttribute("customer") Customer customer) throws Exception {
		ModelAndView view = new ModelAndView("redirect:/customers/?request=update");
		customerService.update(customer);
		return view;
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ModelAndView search(@ModelAttribute("customer") Customer customer) throws Exception {
		ModelAndView view = new ModelAndView("customer");
		List<Customer> customers = customerService.search(customer);
		view.addObject("customers", customers);
		if(customers ==null || customers.size() ==0) {
			view.addObject("message", "Customer not found.");
		}
		return view;
	}
	
	@RequestMapping(value = "/activate/{customerId}", method = RequestMethod.GET)
	public ModelAndView activate(@PathVariable int customerId) throws Exception {
		ModelAndView view = new ModelAndView("redirect:/customers/?request=activate");
		customerService.activate(customerId);
		return view;
	}
	
	@RequestMapping(value = "/deActivate/{customerId}", method = RequestMethod.GET)
	public ModelAndView deActivate(@PathVariable int customerId) throws Exception {
		ModelAndView view = new ModelAndView("redirect:/customers/?request=deactivate");
		customerService.deActivate(customerId);
		return view;
	}

}
